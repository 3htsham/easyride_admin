import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../configs/configs.dart';
import '../../../utils/utils.dart';
import '../../../widgets/widgets.dart';

class ForgetPassScreen extends StatefulWidget {
  const ForgetPassScreen({Key? key}) : super(key: key);

  @override
  State<ForgetPassScreen> createState() => _ForgetPassScreenState();
}

class _ForgetPassScreenState extends State<ForgetPassScreen> {
  TextEditingController emailController = TextEditingController();

  bool isLoading = false;

  _validateAndProceed() async {
    var value = emailController.text;
    if (value.isNotEmpty && value.contains("@") && value.contains(".")) {
      setState(() {
        isLoading = true;
      });

      try {
        await FirebaseAuth.instance.sendPasswordResetEmail(email: value);

        emailController.clear();

        if (!mounted) return;
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("A reset password link sent to you email.")));

        Navigator.of(context).pop();

        setState(() {
          isLoading = false;
        });
      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(e.message ?? 'Error resetting password')));
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        UtilLogger.log('ForgetPassScreen', e);
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Error sending email.")));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.responsiveWidth(80),
                  vertical: SizeConfig.responsiveHeight(150)),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text("Reset Password".toUpperCase(),
                        style: AppTheme.theme!.textTheme.headline1
                            ?.copyWith(fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: SizeConfig.responsiveHeight(20),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: SizeConfig.responsiveWidth(100),
                          vertical: SizeConfig.responsiveHeight(30)),
                      decoration: StyleUtils.getContainerShadowDecoration(),
                      child: Column(
                        children: [
                          TextField(
                            keyboardType: TextInputType.emailAddress,
                            controller: emailController,
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                              hintText: "Email",
                              hintStyle: Theme.of(context).textTheme.bodyText1,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(20),
                          ),
                          isLoading
                              ? SizedBox(
                                  height: SizeConfig.responsiveHeight(40),
                                  child: CircularProgressIndicator(
                                      color: AppTheme.theme!.accentColor))
                              : MySolidButton(
                                  title: 'Send Reset Password Link',
                                  onTap: () {
                                    _validateAndProceed();
                                  },
                                ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(10),
                          ),
                          SimpleButton(
                              onTap: () {
                                if (!isLoading) {
                                  Navigator.of(context).pop();
                                }
                              },
                              title: "Back to Login"),
                        ],
                      ),
                    ),
                  ]))),
    );
  }
}
