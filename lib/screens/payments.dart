import 'package:easy_ride_admin/services/payment_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../configs/configs.dart';
import '../services/services.dart';

class PaymentsScreen extends StatefulWidget {
  const PaymentsScreen({Key? key}) : super(key: key);

  @override
  State<PaymentsScreen> createState() => _PaymentsScreenState();
}

class _PaymentsScreenState extends State<PaymentsScreen> {
  @override
  void initState() {
    super.initState();
    getPayments();
  }

  getPayments() {
    Future.delayed(const Duration(seconds: 1), () {
      context.read<PaymentService>().getPayments();
    });
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<PaymentService>().isLoading;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Payments'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(30),
              vertical: SizeConfig.responsiveHeight(15)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(),
              loading
                  ? SizedBox(
                height: 100,
                width: 40,
                child: Center(
                  child: CircularProgressIndicator(
                    color: AppTheme.theme!.accentColor,
                  ),
                ),
              )
                  : context.watch<PaymentService>().payments.isEmpty
                  ? const Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text('Nothing to show'),
              )
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount:
                context.watch<PaymentService>().payments.length,
                itemBuilder: (ctx, idx) {
                  final payment =
                  context.watch<PaymentService>().payments[idx];
                  return ListTile(
                    title: Text('Amount Paid: ${payment.amountPaid?.toString() ?? ' '} ${context.watch<RentalService>().criteria?.currency ?? ' '}'),
                    subtitle: Text('Distance Travelled: ${payment.ride?.distance?.toStringAsFixed(2) ?? ' '} KMs'),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
