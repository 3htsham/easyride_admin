import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../configs/configs.dart';
import '../../routes/routes.dart';
import '../../services/services.dart';
import '../../utils/utils.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    // await FirebaseAuth.instance.signOut();
    await context.read<FirebaseService>().initialize();
    Future.delayed(const Duration(seconds: 3), () async {
      if (mounted && await context.read<AuthService>().isUserLoggedIn()) {
        //Move to Home Screen
        Navigator.of(context).pushReplacementNamed(RoutePath.home);
      } else {
        Navigator.of(context).pushReplacementNamed(RoutePath.login);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    AppTheme.theme = theme;

    return Container(
      height: SizeConfig.screenHeight,
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(color: AppTheme.theme!.accentColor),
      child: Center(
        child: SizedBox(
            width: SizeConfig.responsiveWidth(400),
            height: SizeConfig.responsiveHeight(400),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10000),
                child: Image.asset(UtilAsset.logoImg))),
      ),
    );
  }
}
