import 'package:easy_ride_admin/models/models.dart';
import 'package:easy_ride_admin/services/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../configs/configs.dart';
import '../widgets/widgets.dart';

class PaymentCriteriaScreen extends StatefulWidget {
  const PaymentCriteriaScreen({Key? key}) : super(key: key);

  @override
  State<PaymentCriteriaScreen> createState() => _PaymentCriteriaScreenState();
}

class _PaymentCriteriaScreenState extends State<PaymentCriteriaScreen> {
  PayCriteria criteria = PayCriteria();
  GlobalKey<FormState> formKey = GlobalKey();

  @override
  void initState() {
    criteria = context.read<RentalService>().criteria!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<RentalService>().isLoading;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Payment Criteria'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(30),
              vertical: SizeConfig.responsiveHeight(15)),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Row(),
                buildTextField(
                    label: 'Currency Label',
                    inputType: TextInputType.text,
                    initialValue:
                        context.watch<RentalService>().criteria?.currency ?? ' ',
                    maxLines: 1,
                    validator: (value) => value != null && value.length > 1
                        ? null
                        : 'Enter a valid label',
                    onSave: (value) {
                      criteria.currency = value;
                    }),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                buildTextField(
                    label: 'Charges Per Kilometer',
                    inputType: TextInputType.phone,
                    initialValue: context
                            .watch<RentalService>()
                            .criteria
                            ?.payPerKm
                            ?.toString() ??
                        ' ',
                    maxLines: 1,
                    validator: (value) => value != null && value.isNotEmpty
                        ? null
                        : 'Enter a valid value',
                    formatters: [
                      FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                    ],
                    onSave: (value) {
                      criteria.payPerKm = int.parse(value.toString());
                    }),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                loading
                    ? SizedBox(
                  height: 40,
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: AppTheme.theme!.accentColor,
                    ),
                  ),
                )
                    : MySolidButton(
                  onTap: () async {
                    if(formKey.currentState?.validate() ?? false) {
                      formKey.currentState!.save();
                      await context.read<RentalService>().updatePaymentCriteria(
                          criteria);
                      setState(() {
                        criteria = context
                            .read<RentalService>()
                            .criteria!;
                      });
                    }
                  },
                  title: 'Update',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buildTextField(
      {required String? Function(String?) validator,
      required String label,
      required Function(dynamic) onSave,
      required TextInputType inputType,
      String? initialValue,
      int maxLines = 1,
      List<TextInputFormatter>? formatters}) {
    return TextFormField(
      validator: validator,
      keyboardType: inputType,
      initialValue: initialValue,
      maxLines: maxLines,
      style: AppTheme.theme!.textTheme.bodyText1,
      inputFormatters: formatters,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        label: Text(label),
        alignLabelWithHint: true,
        labelStyle: AppTheme.theme!.textTheme.caption,
        border: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
        focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(width: 1, color: AppTheme.theme!.accentColor)),
      ),
      onSaved: onSave,
    );
  }
}
