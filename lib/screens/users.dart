import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../configs/configs.dart';
import '../services/services.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({Key? key}) : super(key: key);

  @override
  State<UsersScreen> createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  @override
  void initState() {
    super.initState();
    getUsers();
  }

  getUsers() {
    Future.delayed(const Duration(seconds: 1), () {
      context.read<AuthService>().getUsers();
    });
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<AuthService>().isLoading;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Users'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(30),
              vertical: SizeConfig.responsiveHeight(15)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(),
              loading
                  ? SizedBox(
                      height: 100,
                      width: 40,
                      child: Center(
                        child: CircularProgressIndicator(
                          color: AppTheme.theme!.accentColor,
                        ),
                      ),
                    )
                  : context.watch<AuthService>().users.isEmpty
                      ? const Padding(
                          padding: EdgeInsets.only(top: 40),
                          child: Text('Nothing to show'),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: context.watch<AuthService>().users.length,
                          itemBuilder: (ctx, idx) {
                            final user =
                                context.watch<AuthService>().users[idx];
                            return ListTile(
                              title: Text(user.name),
                              subtitle: Text('${user.email} (${user.id})'),
                            );
                          },
                        )
            ],
          ),
        ),
      ),
    );
  }
}
