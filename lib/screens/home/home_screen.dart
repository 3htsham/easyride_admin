import 'package:easy_ride_admin/routes/routes.dart';
import 'package:easy_ride_admin/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../configs/configs.dart';
import '../../services/services.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    getCriteria();
  }

  getCriteria() {
    Future.delayed(const Duration(seconds: 1), (){
      context.read<RentalService>().getPaymentCriteria();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: SizeConfig.screenWidth,
              height: SizeConfig.screenWidth! * 0.526,
              decoration: StyleUtils.getContainerShadowDecoration(),
              child: Image.asset(
                UtilAsset.banner,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            Text(
              'EASY RIDE ADMIN',
              style: AppTheme.theme!.textTheme.headline1
                  ?.copyWith(fontWeight: FontWeight.w600, wordSpacing: 2),
            ),
            SizedBox(
              height: SizeConfig.responsiveHeight(15),
            ),
            const Divider(),
            SizedBox(
              height: SizeConfig.responsiveHeight(15),
            ),
            Wrap(
              children: [
                buildMenuIcon(
                    title: 'Users',
                    icon: CupertinoIcons.person_3_fill,
                    onTap: () {
                      Navigator.of(context).pushNamed(RoutePath.users);
                    }),
                buildMenuIcon(
                    title: 'Drivers',
                    icon: CupertinoIcons.person_add_solid,
                    onTap: () {
                      Navigator.of(context).pushNamed(RoutePath.drivers);
                    }),
                buildMenuIcon(
                    title: 'Payments',
                    icon: CupertinoIcons.money_rubl_circle,
                    onTap: () {
                      Navigator.of(context).pushNamed(RoutePath.payments);
                    }),
                buildMenuIcon(
                    title: 'Rentals',
                    icon: CupertinoIcons.car_detailed,
                    onTap: () {
                      Navigator.of(context).pushNamed(RoutePath.rentals);
                    }),
                buildMenuIcon(
                    title: 'Payment Criteria',
                    icon: CupertinoIcons.money_dollar,
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed(RoutePath.paymentCriteria);
                    }),
                buildMenuIcon(
                    title: 'LogOut',
                    icon: CupertinoIcons.power,
                    onTap: () async {
                      try {
                        await FirebaseAuth.instance.signOut();
                        if (mounted) {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              RoutePath.login, (route) => false);
                        }
                      } catch (e) {
                        UtilLogger.log('HOME SCREEN', 'LOGOUT: $e');
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }

  buildMenuIcon(
      {required String title,
      required IconData icon,
      required VoidCallback onTap}) {
    return InkWell(
      onTap: () {
        onTap.call();
      },
      child: Container(
        height: SizeConfig.responsiveHeight(130),
        width: SizeConfig.responsiveWidth(450),
        margin: EdgeInsets.symmetric(
            horizontal: SizeConfig.responsiveWidth(20),
            vertical: SizeConfig.responsiveHeight(10)),
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.responsiveWidth(15),
            vertical: SizeConfig.responsiveHeight(10)),
        decoration: StyleUtils.getContainerShadowDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: AppTheme.theme!.accentColor,
              size: 40,
            ),
            SizedBox(
              height: SizeConfig.responsiveHeight(10),
            ),
            Text(
              title,
              style: AppTheme.theme!.textTheme.subtitle2,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
