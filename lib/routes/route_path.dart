class RoutePath {
  static const String splash = '/Splash';
  static const String login = '/Login';
  static const String signup = '/SignUp';
  static const String forgotPass = '/ForgotPassword';
  static const String home = '/Home';

  static const String drivers = '/Drivers';
  static const String users = '/Users';
  static const String payments = '/Payments';
  static const String rentals = '/Rentals';
  static const String paymentCriteria = '/PaymentCriteria';
}
