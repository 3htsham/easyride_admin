import 'package:flutter/material.dart';

import '../screens/screens.dart';
import 'route_path.dart';

class RouteGenerator {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RoutePath.splash:
        return MaterialPageRoute(builder: (context) {
          return const SplashScreen();
        });
      case RoutePath.login:
        return MaterialPageRoute(builder: (context) {
          return const LoginScreen();
        });
      case RoutePath.signup:
        return MaterialPageRoute(builder: (context) {
          return const SignUpScreen();
        });
      case RoutePath.forgotPass:
        return MaterialPageRoute(builder: (context) {
          return const ForgetPassScreen();
        });
      case RoutePath.home:
        return MaterialPageRoute(builder: (context) {
          return const HomeScreen();
        });

      case RoutePath.users:
        return MaterialPageRoute(builder: (context) {
          return const UsersScreen();
        });
      case RoutePath.drivers:
        return MaterialPageRoute(builder: (context) {
          return const DriversScreen();
        });
      case RoutePath.rentals:
        return MaterialPageRoute(builder: (context) {
          return const RentalsScreen();
        });
      case RoutePath.payments:
        return MaterialPageRoute(builder: (context) {
          return const PaymentsScreen();
        });
      case RoutePath.paymentCriteria:
        return MaterialPageRoute(builder: (context) {
          return const PaymentCriteriaScreen();
        });
      default:
        return MaterialPageRoute(
          builder: (context) {
            return Scaffold(
              appBar: AppBar(
                title: const Text('Not Found'),
              ),
              body: Center(
                child: Text('No path for ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
