class UtilAsset {
  static const String images = "assets/images";
  static const String icons = "assets/icons";

  static const String logoImg = "$icons/logo_icon.jpeg";
  static const String banner = '$images/banner.jpeg';
}
