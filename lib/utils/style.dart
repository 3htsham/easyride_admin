import 'package:flutter/material.dart';

import '../configs/configs.dart';

class StyleUtils {

  static BoxDecoration getContainerShadowDecoration({Color? background}) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(color: AppTheme().accentColor(0.4),
          offset: const Offset(1,1),
          blurRadius: 10
        ),
      ],
      borderRadius: BorderRadius.circular(15),
      color: background ?? const Color(0xffffffff),
    );
  }

}