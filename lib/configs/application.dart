import 'package:shared_preferences/shared_preferences.dart';

import '../models/models.dart';

class Application {
  static const String appName = 'Easy Ride';
  static const bool debug = true;
  static const String version = '1.0.0';
  static SharedPreferences? prefs;
  static MyUser? user;

  ///Singleton factory v
  static final Application _instance = Application._internal();

  factory Application() {
    return _instance;
  }

  Application._internal();
}
