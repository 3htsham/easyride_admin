import 'package:flutter/material.dart';

class AppTheme {

  static ThemeData? theme;

  Color purple = const Color(0xFFd52328);
  Color orangish = const Color(0xFFff5722);
  Color mytextColor = const Color(0xFF303952);
  Color myfocusColor = const Color(0xFF969BA7);
  static Color dividerColor = const Color(0xFFEAEBED);
  static Color highlightColor = const Color(0xFFEEE7ED);
  static Color textFormFieldColor = const Color(0xFFF4F5F7);
  static Color borderColor = const Color(0xFF303A51);


  /// Heading1 - 22 Bold Semibold PingFing
  /// Heading2 - 24 - Medium -- (Not from design)
  /// Heading3 - 22 - Bold
  /// Heading4 - 20 - Bold
  /// subtitle1 - 18 - Bold (DONE)
  /// subtitle2 - 15 - Medium (DONE)
  /// bodyText1 - 15 - Regular (DONE)
  /// bodyText2 - 14 - Regular (DONE)
  /// caption - 12 - Regular (DONE)

  final Color _mainColor = const Color(0xFFE9724C);
  final Color _primaryColor = const Color(0xFFE9724C);
  final Color _secondColor = const Color(0xFFA20A1D);///
  final Color _secondLightColor = const Color(0xFFB53B4A);///
  final Color _accentColor = const Color(0xFFd52328);///
  final Color _scaffoldColor = const Color(0xFFF6F6F6);///
  final Color _focusColor = const Color(0xFF969BA7);
  final Color _captionColor = const Color(0xFF969BA7);
  final Color _bodyTextColor = const Color(0xFF303952);///

  final Color _scaffoldDarkColor = const Color(0xFF12151F);///
  final Color _focusDarkColor = const Color(0xFF1E212C);
  final Color _captionDarkColor = const Color(0xFF969BA7);
  final Color _bodyTextDarkColor = const Color(0xFFF6F6F6);///
  final Color _accentDarkColor = const Color(0xffff070f);///

  Color bottomNavColor = const Color(0xFF1E212C);
  Color translationSelectorColor = const Color(0xFF000000);

  Color mainColor(double opacity) {
    return _mainColor.withOpacity(opacity);
  }

  Color primaryColor(double opacity) {
    return _primaryColor.withOpacity(opacity);
  }

  Color secondColor(double opacity) {
    return _secondColor.withOpacity(opacity);
  }

  Color secondLightColor(double opacity) {
    return _secondLightColor.withOpacity(opacity);
  }

  Color accentColor(double opacity) {
    return _accentColor.withOpacity(opacity);
  }

  Color scaffoldColor(double opacity) {
    return _scaffoldColor.withOpacity(opacity);
  }

  Color focusColor(double opacity) {
    return _focusColor.withOpacity(opacity);
  }

  Color captionColor(double opacity) {
    return _captionColor.withOpacity(opacity);
  }

  Color bodyTextColor(double opacity) {
    return _bodyTextColor.withOpacity(opacity);
  }

  Color cardColor(double opacity) {
    return Colors.black.withOpacity(opacity);
  }

  ///Dark Colors
  Color accentDarkColor(double opacity) {
    return _accentDarkColor.withOpacity(opacity);
  }

  Color scaffoldDarkColor(double opacity) {
    return _scaffoldDarkColor.withOpacity(opacity);
  }

  Color focusDarkColor(double opacity) {
    return _focusDarkColor.withOpacity(opacity);
  }

  Color captionDarkColor(double opacity) {
    return _captionDarkColor.withOpacity(opacity);
  }

  Color bodyTextDarkColor(double opacity) {
    return _bodyTextDarkColor.withOpacity(opacity);
  }

  Color cardDarkColor(double opacity) {
    return translationSelectorColor.withOpacity(opacity);
  }

  Color disabledDarkColor(double opacity) {
    return _captionDarkColor.withOpacity(opacity);
  }

  LinearGradient topLeftBottomRightGradient() {
    return LinearGradient(
        colors: [
          purple,
          orangish
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
    );
  }

  LinearGradient bottomRightTopLeftGradient() {
    return LinearGradient(
        colors: [
          purple,
          orangish
        ],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft
    );
  }

  List<BoxShadow> getShadowEffect() {
    return [
      BoxShadow(
          color: Colors.black.withOpacity(0.08),
          offset: const Offset(0, 6),
          blurRadius: 50
      )
    ];
  }
}