import 'package:easy_ride_admin/services/payment_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app/app.dart';
import 'services/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FirebaseMessaging.instance.setAutoInitEnabled(true);
  // FirebaseMessaging?.onBackgroundMessage(myBackgroundMessageHandler);


  final providers = MultiProvider(
    providers: [
      ChangeNotifierProvider(
        lazy: false,
        create: (ctx) => FirebaseService(ctx),
      ),
      ChangeNotifierProvider(
        lazy: false,
        create: (ctx) => AuthService(ctx),
      ),
      ChangeNotifierProvider(
        lazy: false,
        create: (ctx) => RentalService(ctx),
      ),
      ChangeNotifierProvider(
        lazy: false,
        create: (ctx) => PaymentService(ctx),
      ),
    ],
    child: const App()
  );
  runApp(providers);
}
