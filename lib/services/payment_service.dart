import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride_admin/models/models.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/payment_criteria.dart';
import '../utils/utils.dart';
import 'services.dart';

class PaymentService extends ChangeNotifier {
  List<PaymentModel> payments = [];
  bool isLoading = false;

  BuildContext context;

  PaymentService(this.context);

  getPayments() async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    final payCollection = fireStore.collection(DatabaseUtil.payments);
    payments.clear();
    isLoading = true;
    notifyListeners();
    final querySnap = await payCollection.get();
    final docs = querySnap.docs;
    if (docs.isNotEmpty) {
      for (int i = 0; i < docs.length; i++) {
        final doc = docs[i];
        if (doc.exists) {
          final data = doc.data();
          final pay = PaymentModel.fromJson(data);
          final ride = await fireStore
              .collection(DatabaseUtil.rides)
              .doc(pay.rideId)
              .get();
          final shareable = await fireStore
              .collection(DatabaseUtil.shareableRides)
              .doc(pay.rideId)
              .get();
          if (ride.exists) {
            pay.ride = RideDetails.fromJson(ride.data()!);
          } else {
            pay.ride = RideDetails.fromJson(shareable.data()!);
          }
          pay.ride?.distance = CalUtils.calculateDistance(pay.ride!.pickup!.latitude!,
              pay.ride!.pickup!.longitude!,
              pay.ride!.dropOff!.latitude!, pay.ride!.dropOff!.longitude!);
          payments.add(pay);
          notifyListeners();
        }
      }
    }
    isLoading = false;
    notifyListeners();
  }
}
