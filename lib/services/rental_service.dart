import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import '../models/models.dart';
import '../utils/utils.dart';
import 'services.dart';

class RentalService extends ChangeNotifier {
  bool isLoading = false;
  var uuid = const Uuid();
  List<RentalModel> rentals = [];
  BuildContext context;

  PayCriteria? criteria;

  RentalService(this.context);

  getRentals() async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    rentals.clear();
    isLoading = true;
    notifyListeners();
    final querySnap = await fireStore.get();
    final docs = querySnap.docs;
    if (docs.isNotEmpty) {
      for (int i = 0; i < docs.length; i++) {
        final doc = docs[i];
        if (doc.exists) {
          final data = doc.data();
          final rentalModel = RentalModel.fromJson(data);
          rentals.add(rentalModel);
          notifyListeners();
        }
      }
    }
    isLoading = false;
    notifyListeners();
  }

  deleteRental(RentalModel rental) async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.rentals);
    rentals.clear();
    isLoading = true;
    notifyListeners();
    await fireStore.doc(rental.id).delete();
    getRentals();
  }

  getPaymentCriteria() async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.config);
    criteria = null;
    isLoading = true;
    notifyListeners();
    final docSnap = await fireStore.doc(DatabaseUtil.paymentCriteria).get();
    if (docSnap.exists) {
      final data = docSnap.data() as Map<String, dynamic>;
      final criteriaModel = PayCriteria.fromJson(data);
      criteria = criteriaModel;
      notifyListeners();
    }
    isLoading = false;
    notifyListeners();
  }

  updatePaymentCriteria(PayCriteria criteria) async {
    isLoading = true;
    notifyListeners();
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.config);
    await fireStore.doc(DatabaseUtil.paymentCriteria).set(criteria.toMap());
    await getPaymentCriteria();
    return;
  }
}
