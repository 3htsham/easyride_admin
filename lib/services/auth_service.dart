import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../configs/configs.dart';
import '../models/models.dart';
import '../utils/utils.dart';
import 'services.dart';

class AuthService extends ChangeNotifier {
  bool isLoading = true;
  List<MyUser> users = [];
  List<MyUser> drivers = [];

  User? currentUser;

  BuildContext context;

  AuthService(this.context);


  Future<bool> isUserLoggedIn() async {
    currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      Application.user = await getUserDetails();
      final user = Application.user;
    }
    return currentUser != null;
  }

  Future<MyUser> getUserDetails() async {
    final authUser = FirebaseAuth.instance.currentUser;
    CollectionReference users = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.users);
    final userDetails = await users.doc(authUser!.uid).get();
    Map<String, dynamic> data = userDetails.data() as Map<String, dynamic>;
    return MyUser.fromJson(data);
  }

  Future<void> addUserToFirestore(
      MyUser myUser, User user, String updatedName) async {
    FirebaseFirestore firStore = FirebaseFirestore.instance;
    CollectionReference users = firStore.collection(DatabaseUtil.users);
    myUser.email = user.email!.toLowerCase();
    myUser.id = user.uid;
    myUser.isActive = false;
    await users.doc(user.uid).set(myUser.toMap());
    UtilLogger.log('AuthService', "User Added");
  }

  getUsers() async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.users);
    users.clear();
    isLoading = true;
    notifyListeners();
    final querySnap = await fireStore.get();
    final docs = querySnap.docs;
    if (docs.isNotEmpty) {
      for (int i = 0; i < docs.length; i++) {
        final doc = docs[i];
        if (doc.exists) {
          final data = doc.data();
          final user = MyUser.fromJson(data);
          if (user.type != 'admin') {
            users.add(user);
            notifyListeners();
          }
        }
      }
    }
    isLoading = false;
    notifyListeners();
  }

  getDrivers() async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.drivers);
    drivers.clear();
    isLoading = true;
    notifyListeners();
    final querySnap = await fireStore.get();
    final docs = querySnap.docs;
    if (docs.isNotEmpty) {
      for (int i = 0; i < docs.length; i++) {
        final doc = docs[i];
        if (doc.exists) {
          final data = doc.data();
          final user = MyUser.fromJson(data);
          drivers.add(user);
          notifyListeners();
        }
      }
    }
    isLoading = false;
    notifyListeners();
  }

  toggleDriver(MyUser driver, bool isActive) async {
    final fireStore = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.drivers);
    await fireStore
        .doc(driver.id)
        .set({'isActive': isActive}, SetOptions(merge: true));
    getDrivers();
  }
}
