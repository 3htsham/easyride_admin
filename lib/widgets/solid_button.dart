import 'package:flutter/material.dart';

import '../configs/configs.dart';

class MySolidButton extends StatelessWidget {
  final String title;
  final VoidCallback? onTap;
  const MySolidButton({Key? key, this.title = 'None', this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.responsiveHeight(40),
      child: ElevatedButton(
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          onTap?.call();
        },
        style: ElevatedButton.styleFrom(
            primary: AppTheme.theme!.accentColor,
            shape: const StadiumBorder(),
            shadowColor: Colors.black),
        child: Center(
          child: Text(
            title,
          ),
        ),
      ),
    );
  }
}
